from numbers import Number
from copy import deepcopy
from fractions import Fraction

class Matrix:
    def __init__(self, entryList = None, field = Fraction):
        self.field = field
        if (entryList) and type(entryList) is list:
            self.matrix = entryList
            self.rows = len(entryList)
            self.columns = len(entryList[0])
            for j,i in self:
                self[j,i] = field(self[j,i])

        else:
            self.matrix = []
            self.rows = 0
            self.columns = 0

        self.rank = None
        self.det = None
        self.inv = None

    def __getitem__(self, pos):
        if type(pos) is tuple:
            m, n = pos
            return self.matrix[m][n]
        elif type(pos) is int:
            return self.matrix[pos]

    def __setitem__(self, pos, value):
        if type(pos) is tuple:
            m, n = pos
            self.matrix[m][n] = self.field(value)
        elif type(pos) is int and type(value) is list and isinstance[list[0],self.field]:
            self.matrix[pos] = value

    def __str__(self):
        outputString = ""
        for j,i in self:
            if i == 0:
                outputString += "("
            outputString += str(self[j,i])
            outputString += " "*(len(self) - len(str(self[j,i])))
            if i < self.columns - 1:
                outputString += " | "
            if i == self.columns -1:
                outputString += ")\n"
        return outputString

    def __repr__(self):
        STR = [[str(self[j,i]) for i in range(self.columns)] for j in range(self.rows)]
        out = "Matrix(" + str(STR) + ")"
        return out

    def __len__(self):
        maxLength = 0
        for j,i in self:
            currentLength = len(str(self[j,i]))
            if currentLength > maxLength:
                maxLength = currentLength
                self.length = maxLength
        return maxLength

    def __iter__(self):
        for j in range(self.rows):
            for i in range(self.columns):
                yield j,i

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.matrix == other.matrix
        else:
            return False

    @staticmethod
    def emptyMatrix(rows, columns, field = Fraction):
        matrixList = [[field(0) for i in range(columns)] for j in range(rows)]
        return Matrix(matrixList)

    @staticmethod
    def identityMatrix(n, field = Fraction):
        E = Matrix.emptyMatrix(n,n)
        for i in range(n):
            E[i,i] = field(1)
        return E

    @staticmethod
    def permutationMatrix(n, rowA, rowB):
        P = Matrix.identityMatrix(n)
        P[rowA], P[rowB] = P[rowB], P[rowA]
        return P

    @staticmethod
    def multiplyRowMatrix(n, row, factor, field = Fraction):
        M = Matrix.identityMatrix(n)
        M[row,row] = M[row,row] * field(factor)
        return M

    @staticmethod
    def addMultipleRow(n, rowFrom, rowTo, factor, field = Fraction):
        G = Matrix.identityMatrix(n)
        G[rowTo, rowFrom] = field(factor)
        return G


    def __add__(self, other):
        C = self.emptyMatrix(self.rows, self.columns)
        if type(other) is type(self):
            if self.rows == other.rows and self.columns == other.columns:
                for j,i in self:
                    C[j,i] = self[j,i] + other[j,i]
            return C
        if isinstance(other,Number):
            for j,i in self:
                C[j,i] = self[j,i] + self.field(other)
            return C

    def __radd__(self, other):
        C = self.emptyMatrix(self.rows, self.columns)
        if isinstance(other, Number):
            for j,i in self:
                C[j,i] = self.field(other) + self[j,i]
            return C

    def __sub__(self, other):
        C = self.emptyMatrix(self.rows, self.columns)
        if type(other) is type(self):
            if self.rows == other.rows and self.columns == other.columns:
                for j,i in self:
                    C[j,i] = self[j,i] - other[j,i]
            return C
        elif isinstance(other, Number):
            for j,i in self:
                C[j,i] = self[j,i] - self.field(other)
            return C

    def __rsub__(self, other):
        C = self.emptyMatrix(self.rows, self.columns)
        if isinstance(other, Number):
            for j,i in self:
                C[j,i] = self.field(other) - self[j,i]
            return C

    def __mul__(self, other):
        if type(other) is type(self) and self.columns == other.rows:
            C = self.emptyMatrix(self.rows, other.columns)
            for j in range(self.rows):
                for i in range(other.columns):
                    total = 0
                    for k in range(self.columns):
                        total += self[j,k] * other[k,i]
                    C[j,i] = total
            return C
        elif isinstance(other, Number):
            C = self.emptyMatrix(self.rows, self.columns)
            for j,i in self:
                C[j,i] = self[j,i] * self.field(other)
            return C

    def __rmul__(self, other):
        C = self.emptyMatrix(self.rows, self.columns)
        if isinstance(other, Number):
            for j,i in self:
                C[j,i] = self[j,i] * self.field(other)
            return C

    def __truediv__(self, other):
        if type(self) == type(other):
            return self * other.inverse(cache = True)
        elif isinstance(other, Number):
            return self * (1 / other)

    def __pow__(self, power):
        E = Matrix.identityMatrix(self.rows)
        if power > 0:
            for i in range(power):
                E = E * self
        elif power < 0:
            for i in range(abs(power)):
                E = E * self.inverse(cache=True)
        return E

    def transpose(self):
        C = self.emptyMatrix(self.columns, self.rows)
        for j,i in self:
            C[i,j] = self[j,i]
        return C

    def rang(self):
        self.treppenNormalForm()
        return self.rank

    def determinante(self):
        self.treppenNormalForm()
        return self.det


    def treppenNormalForm_matrixMul(self):
        TNF = deepcopy(self)
        pivotPosition = 0
        for i in range(min(TNF.columns, TNF.rows)):
            # wir iterieren über die Spalten,
            for j in range(TNF.rows):
                # und dann über die Zeilen.
                print(f"-------\nDas Element TNF[{j},{i}] ist {TNF[j,i]}\n--------")
                if TNF[j,i] == 0:
                    # bei einer Null gehe einfach in die nächste Spalte.
                    continue
                elif TNF[j,i] != 0 and TNF[pivotPosition,i] == 0:
                    print(f"Tausche: {i} und {j}")
                    TNF = TNF.permutationMatrix(TNF.rows, j, pivotPosition) * TNF
                    print(TNF)
                elif TNF[j,i] != 0 and j != pivotPosition:
                    print(f"Addiere {-TNF[j,i]/TNF[pivotPosition,i]}-fache der {pivotPosition}-ten Zeile zur {j}-ten Zeile.")
                    TNF = TNF.addMultipleRow(TNF.rows, pivotPosition, j, -TNF[j,i]/TNF[pivotPosition,i]) * TNF
                    print(TNF)
                elif j == pivotPosition and TNF[j,i] != 1:
                    print(f"Normalisiere {pivotPosition}-te Zeile.")
                    TNF = TNF.multiplyRowMatrix(TNF.rows, j, 1/TNF[j,i]) * TNF
                    print(TNF)
            if TNF[pivotPosition, i] != 0:
                pivotPosition += 1
        self.rank = pivotPosition
        return TNF

    def treppenNormalForm(self, debug = False):
        def P(M, i, j):
            M.matrix[i] ,M.matrix[j] = M.matrix[j], M.matrix[i]
            return M

        def G(M, rowFrom, rowTo, factor):
            for k in range(len(M.matrix[rowTo])):
                M[rowTo,k] += M[rowFrom,k] * factor
            return M

        def M(M, row, factor):
            for k in range(len(M.matrix[row])):
                M[row,k] = M[row,k] * factor
            return M

        TNF = deepcopy(self)
        pivotPosition = 0
        det = self.field(1)
        for i in range(min(TNF.columns, TNF.rows)):

            # finde Zeile mit größtem Starteintrag und tausche mit der nächsten Pivotzeile
            biggestPivot, index = 0, pivotPosition
            for j in range(pivotPosition, TNF.rows):
                if abs(TNF[j,i]) > biggestPivot:
                    index = j
                    biggestPivot = abs(TNF[j,i])
            if index != pivotPosition:
                if debug: print(f"Tausche: {i} und {j}")
                P(TNF, pivotPosition, index)
                det *= -1
                if debug: print(TNF)

            # Normalisiere Pivotzeile
            if (TNF[pivotPosition, i] != 0 and TNF[pivotPosition, i] != 1):
                if debug: print(f"Normalisiere {pivotPosition}-te Zeile.")
                factor = 1 / TNF[pivotPosition,i]
                det *= TNF[pivotPosition,i]
                M(TNF, pivotPosition, factor)
                if debug: print(TNF)

            # Stelle Nullen über und unter der Pivotzeile her.
            for j in range(TNF.rows):
                if TNF[j,i] == 0:
                    continue
                elif TNF[j,i] != 0 and j != pivotPosition and TNF[pivotPosition,i] != 0:
                    if debug: print(f"Addiere {-TNF[j,i]/TNF[pivotPosition,i]}-fache der {pivotPosition}-ten Zeile zur {j}-ten Zeile.")
                    factor = - TNF[j,i] / TNF[pivotPosition,i]
                    G(TNF, pivotPosition, j, factor)
                    if debug: print(TNF)
            if TNF[pivotPosition, i] != 0:
                pivotPosition += 1
        self.rank = pivotPosition
        if self.rank < self.rows:
            self.det = self.field(0)
        else:
            self.det = det
        return TNF

    def inverse(self, cache = False):
        if self.inv != None:
            return self.inv

        def P(M, i, j):
            M.matrix[i] ,M.matrix[j] = M.matrix[j], M.matrix[i]
            return M

        def G(M, rowFrom, rowTo, factor):
            for k in range(len(M.matrix[rowTo])):
                M[rowTo,k] += M[rowFrom,k] * factor
            return M

        def M(M, row, factor):
            for k in range(len(M.matrix[row])):
                M[row,k] = M[row,k] * factor
            return M

        TNF = deepcopy(self)
        ID = Matrix.identityMatrix(self.columns)
        pivotPosition = 0
        for i in range(min(TNF.columns, TNF.rows)):

            # finde Zeile mit größtem Starteintrag und tausche mit der nächsten Pivotzeile
            biggestPivot, index = 0, pivotPosition
            for j in range(pivotPosition, TNF.rows):
                if abs(TNF[j,i]) > biggestPivot:
                    index = j
                    biggestPivot = abs(TNF[j,i])
            if index != pivotPosition:
                P(TNF, pivotPosition, index)
                P(ID, pivotPosition, index)

            # Normalisiere Pivotzeile
            if (TNF[pivotPosition, i] != 0 and TNF[pivotPosition, i] != 1):
                factor = 1 / TNF[pivotPosition,i]
                M(TNF, pivotPosition, factor)
                M(ID, pivotPosition, factor)

            # Stelle Nullen über und unter der Pivotzeile her.
            for j in range(TNF.rows):
                if TNF[j,i] == 0:
                    continue
                elif TNF[j,i] != 0 and j != pivotPosition and TNF[pivotPosition,i] != 0:
                    factor = - TNF[j,i] / TNF[pivotPosition,i]
                    G(TNF, pivotPosition, j, factor)
                    G(ID, pivotPosition, j, factor)
            if TNF[pivotPosition, i] != 0:
                pivotPosition += 1
        self.rank = pivotPosition
        if pivotPosition == self.columns:
            if cache == True:
                self.inv = ID
            return ID
        else:
            print("Matrix is not invertible.")

    def getRow(self, column_index):
        result = []
        for i in range(self.rows):
            result.append(self[i,column_index])
        return result

    def setRow(self, column_index, newRow):
        for i in range(self.rows):
            self[i,column_index] = newRow[i];


    def solveLGS(self, rhs):
        coefficient_det = self.determinante();
        if coefficient_det == 0:
            print("This LGS does not have a clear solution!")

        result = [];

        for i in range(self.rows):
            tmp = deepcopy(self)
            x_i = 1/coefficient_det
            tmp.setRow(i, rhs)
            x_i *= tmp.determinante()
            result.append(x_i)

        return result
