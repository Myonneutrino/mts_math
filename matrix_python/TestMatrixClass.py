import unittest
from Matrix import Matrix

class TestMatrixClass(unittest.TestCase):
    def test_inverse(self):
        T = Matrix([[2,7,1],[6,6,4],[0,5,3]])
        self.assertEqual(T.inverse() * T, Matrix.identityMatrix(3))

        T_inverse = Matrix([['1/50','4/25','-11/50'],['9/50','-3/50','1/50'],['-3/10','1/10','3/10']])
        self.assertEqual(T.inverse(), T_inverse)




if __name__ == '__main__':
    unittest.main()
