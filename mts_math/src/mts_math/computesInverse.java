package mts_math;

public interface computesInverse {
     Matrix getInverse();
     void solve(Matrix aMatrix);
}
