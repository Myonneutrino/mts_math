package mts_math;

public class Program {
    public static void main(String[] args) {
        try {
            Matrix M = new Matrix(new double[][]{{-77, -70, 89}, {297, -529, 278}, {-435, 330, -33}});
            M.printMatrix();
            System.out.println("Inverse: ");
            M.inverse().printMatrix();

            System.out.println();

            Matrix N = new Matrix(new double[][] {{1,2,3},{0,5,6},{7,8,0}});
            N.printMatrix();
            System.out.println("Inverse: ");
            N.inverse().printMatrix();
        } catch (InputException e) {
            System.out.println(e.getMessage());
        }
    }
}
