package mts_math;

public class MatrixOperations {
    // TODO add other matrix operations
    private computesDeterminante detAlgorithm;
    private computesInverse invAlgorithm;
    private computesRank rankAlgorithm;

    public MatrixOperations(computesRank rankAlg, computesInverse invAlg, computesDeterminante detAlg) {
        this.rankAlgorithm = rankAlg;
        this.invAlgorithm = invAlg;
        this.detAlgorithm = detAlg;
    }

    public MatrixOperations(){
        // The standard implementations for now.
        TreppenNormalForm tnf = new TreppenNormalForm();
        this.rankAlgorithm = tnf;
        this.invAlgorithm = tnf;
        this.detAlgorithm = tnf;
    }

    public computesDeterminante getDetAlgorithm(){
        return this.detAlgorithm;
    }
    public computesInverse getInvAlgorithm() {
        return this.invAlgorithm;
    }
    public computesRank getRankAlgorithm() {
        return this.rankAlgorithm;
    }
}
