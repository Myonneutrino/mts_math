package mts_math;

public interface computesRank {
    int getRank();
    void solve(Matrix aMatrix);
}
