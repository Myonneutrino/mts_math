package mts_math;

import java.util.Objects;

public class TreppenNormalForm implements computesDeterminante, computesRank, computesInverse {
    private int rank = -1;
    private Fraction det;
    private Matrix tnf;
    private Matrix inverse;

    TreppenNormalForm() {
    }

    public void solve(Matrix aMatrix) {
        try {
            this.tnf = new Matrix(aMatrix);
            int pivotPosition = 0;
            Fraction det = new Fraction(1L, 1L);
            Matrix ID = Matrix.id(this.tnf.rows());

            for (int column = 0; column < this.tnf.columns() && column < this.tnf.rows(); column++) {
                if (pivotPosition != this.biggestElement(this.tnf, pivotPosition, column)) {
                    det = Fraction.mul(det, -1D);
                    int biggestElementIndex = this.biggestElement(this.tnf, pivotPosition, column);
                    this.switchRows(this.tnf, pivotPosition, biggestElementIndex);
                    this.switchRows(ID, pivotPosition, biggestElementIndex);
                }
                if ( ! Fraction.isEqual(this.tnf.getElement(pivotPosition, column), new Fraction(0)) && ! Fraction.isEqual(this.tnf.getElement(pivotPosition, column), new Fraction(1))) {
                    det = Fraction.mul(this.tnf.getElement(pivotPosition, column), det);
                    Fraction factor = new Fraction(this.tnf.getElement(pivotPosition, column).getDenominator(), this.tnf.getElement(pivotPosition, column).getNominator());
                    this.multiplyRow(this.tnf, pivotPosition, factor);
                    this.multiplyRow(ID, pivotPosition, factor);
                }
                for (int row = 0; row < this.tnf.rows(); row++) {
                    if (Fraction.isEqual(this.tnf.getElement(row, column), new Fraction(0))) {
                        continue;
                    }
                    if (row != pivotPosition && ! Fraction.isEqual(this.tnf.getElement(pivotPosition, column), new Fraction(0))) {
                        Fraction factor = Fraction.div(Objects.requireNonNull(Fraction.sub(new Fraction(0), this.tnf.getElement(row, column))), this.tnf.getElement(pivotPosition, column));
                        this.addMultipleOfRow(this.tnf, pivotPosition, row, factor);
                        this.addMultipleOfRow(ID, pivotPosition, row, factor);
                    }
                }
                if (! Fraction.isEqual(this.tnf.getElement(pivotPosition, column), new Fraction(0))) {
                    pivotPosition += 1;
                }
            }
            this.rank = pivotPosition;
            this.det = det;
            this.inverse = ID;
        } catch (InputException e) {
            // This shouldn't happen.
            System.out.println(e.getMessage());
        }
    }
    public void switchRows(Matrix aMatrix, int row1, int row2) throws InputException{
        if (row1 < 0 || row2 < 0 || row1 >= aMatrix.rows() || row2 >= aMatrix.rows()){
            throw new InputException("Index out of bound!");
        }
        Fraction[] tmp = aMatrix.getRow(row1);
        aMatrix.setRow(row1, aMatrix.getRow(row2));
        aMatrix.setRow(row2, tmp);
    }

    public void addMultipleOfRow(Matrix aMatrix, int ofRow, int toRow, Fraction value) throws InputException {
        if (ofRow < 0 || toRow < 0 || ofRow >= aMatrix.rows() || toRow >= aMatrix.rows()){
            throw new InputException("Index out of bound!");
        }
        for (int column = 0; column < aMatrix.columns(); column++) {
            aMatrix.setElement(toRow, column, Fraction.add(Fraction.mul(aMatrix.getElement(ofRow, column), value), aMatrix.getElement(toRow, column)));
        }
    }

    public void multiplyRow(Matrix aMatrix, int row, Fraction value) throws InputException{
        if (row < 0 || row >= aMatrix.rows()){
            throw new InputException("Index out of bound!");
        }
        for (int column = 0; column < aMatrix.columns(); column++) {
            aMatrix.setElement(row,column, Fraction.mul(aMatrix.getElement(row,column), value));
        }
    }

    public int biggestElement(Matrix aMatrix, int startRow, int column) throws InputException {
        int result = startRow;
        for (int row = startRow; row < aMatrix.rows(); row++) {
            if (Fraction.isGreaterThan(aMatrix.getElement(row,column), aMatrix.getElement(result, column))) {
                result = row;
            }
        }
        return result;
    }

    public int getRank() {
        return this.rank;
    }

    public Fraction getDet() {
        if (this.rank != this.tnf.rows()){
            return new Fraction(0);
        } else {
            return this.det;
        }
    }

    public Matrix getTnf() {
        return this.tnf;
    }

    public Matrix getInverse() {
        if (this.rank != this.tnf.rows()){
            throw new OperationException("Matrix is not invertible.");
        } else {
            return this.inverse;
        }
    }
}
