package mts_math;

public class Vector {
    private Fraction[] vector;
    private int dimension;

    public Vector(Fraction[] vector) {
        this.dimension = vector.length;
        this.vector = vector;
    }

    public Vector(double[] vector) {
        this.dimension = vector.length;
        this.vector = new Fraction[this.dimension];

        for (int i = 0; i < this.dimension; i++) {
            this.vector[i] = new Fraction(vector[i]);
        }
    }

    public Vector(int dimension) {
        this.dimension = dimension;
        this.vector = new Fraction[this.dimension];

        for (int i = 0; i < this.dimension; i++) {
            try {
                this.vector[i] = new Fraction(0L, 1L);
            } catch (InputException e) {
                // do nothing.
            }
        }
    }

    public Matrix transpose() {

    }

    public Vector normal() {

    }

    public Vector length() {

    }

    public static Vector add(Vector vector1, Vector vector2) {

    }

    public static Fraction mul(Vector vector1, Vector vector2) {

    }

    public static Vector mul(Vector vector, Fraction fraction) {

    }

    public static Vector cross_mul(Vector vector1, Vector vector2) {

    }

    public static Vector sub(Vector vector1, Vector vector2) {

    }

    public static boolean isLinearlyDependent(Vector[] array_of_vectors) {

    }

    public static boolean isCollinear(Vector[] array_of_vectors) {

    }



}
