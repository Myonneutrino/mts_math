package mts_math;

public interface computesDeterminante {
     Fraction getDet();
     void solve(Matrix aMatrix);
}
