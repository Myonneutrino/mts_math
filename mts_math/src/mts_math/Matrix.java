package mts_math;

public class Matrix {

	private Fraction[][] array;
	private int rows;
	private int columns;
	private MatrixOperations ops;

	public Matrix(Fraction[][] anArray) throws InputException {
	    checkInputArray(anArray);

		this.array = anArray;
		this.rows = anArray.length;
		this.columns = anArray[0].length;
		this.ops = new MatrixOperations();
	}

	public Matrix(double[][] anArray) throws InputException {
		checkInputArray(anArray);

		this.rows = anArray.length;
		this.columns = anArray[0].length;
		this.array = new Fraction[this.rows][this.columns];

		for (int row = 0; row < this.rows; row++) {
			for (int column = 0; column < this.columns; column++) {
				this.array[row][column] = new Fraction(anArray[row][column]);
			}
		}

		this.ops = new MatrixOperations();
	}

	public Matrix(Fraction[][] anArray, MatrixOperations matOps) throws InputException {
		this(anArray);
		this.ops = matOps;
	}
	
	public Matrix(int rows, int columns) throws InputException {
		// creates m x n zero matrix
		if (columns <= 0 || rows <= 0) {
			throw new InputException("Parameters need to be a positive integer.");
		}
		this.array = new Fraction[rows][columns];
		this.rows = rows;
		this.columns = columns;
		for (int row = 0; row < this.rows; row++) {
			for (int column = 0; column < this.columns; column++) {
				this.setElement(row, column, new Fraction(0));
			}
		}
		this.ops = new MatrixOperations();
	}

	public Matrix(int rows, int columns, MatrixOperations matOps) throws InputException {
		this(rows, columns);
		this.ops = matOps;
	}

	private void checkInputArray(Fraction[][] array) throws InputException {
		if (array.length == 0) {
			throw new InputException("Input Array is empty!");
		} else if (array[0].length == 0) {
			throw new InputException("Input Array is empty!");
		}
		for (Fraction[] row : array) {
			if (row.length != array[0].length) {
				throw new InputException("row length should be " + array[0].length + "but a row has length " + row.length);
			}
		}
	}

	private void checkInputArray(double[][] array) throws InputException {
		if (array.length == 0) {
			throw new InputException("Input Array is empty!");
		} else if (array[0].length == 0) {
			throw new InputException("Input Array is empty!");
		}
		for (double[] row : array) {
			if (row.length != array[0].length) {
				throw new InputException("row length should be " + array[0].length + "but a row has length " + row.length);
			}
		}
	}

	public Matrix(Matrix that) throws InputException {
		this(that.rows(), that.columns(), that.getOps());
		for (int row = 0; row < this.rows; row++) {
			for (int column = 0; column < this.columns; column++) {
				this.setElement(row, column, new Fraction(that.getElement(row,column)));
			}
		}
	}

	public MatrixOperations getOps(){
		return this.ops;
	}

	public int rows(){
		return this.rows;
	}

	public int columns(){
		return this.columns;
	}

	public void setArray(Fraction[][] array) throws InputException {
		checkInputArray(array);
		this.array = array;
	}
	
	public Fraction[][] getArray() {
		return this.array;
	}

	public void setElement(int row, int column, Fraction value) throws InputException {
		if (row < 0 || row >= this.rows || column < 0 || column >= this.columns) {
			throw new InputException("Indices are out of bound!");
		}
		this.array[row][column] = value;
	}
	
	public Fraction getElement(int row, int column) throws InputException {
		if (row < 0 || row >= this.rows || column < 0 || column >= this.columns) {
			throw new InputException("Indices are out of bound!");
		}
		return this.array[row][column];
	}
	
	public void setRow(int rowIndex, Fraction[] rowArray) throws InputException {
		if (rowIndex < 0 || rowIndex >= this.rows) {
			throw new InputException("Index is out of bound!");
		} else if (rowArray.length != this.columns){
			throw new InputException("Row length is wrong!");
		}

		for (int columnIndex = 0; columnIndex < this.columns; columnIndex++) {
			this.setElement(rowIndex, columnIndex, rowArray[columnIndex]);
		}
	}
		
	public Fraction[] getRow(int rowIndex) throws InputException {
		if (rowIndex < 0 || rowIndex >= this.rows) {
			throw new InputException("Index is out of bound!");
		}
		Fraction[] result = new Fraction[this.columns];
		System.arraycopy(this.array[rowIndex], 0, result, 0, this.columns);
		return result;
	}
	
	public void setColumn(int columnIndex, Fraction[] columnArray) throws InputException {
		if (columnIndex < 0 || columnIndex >= this.columns) {
			throw new InputException("Index is out of bound!");
		} else if (columnArray.length != this.rows){
			throw new InputException("Column length is wrong!");
		}
		for (int rowIndex = 0; rowIndex < this.rows; rowIndex++) {
			this.setElement(rowIndex, columnIndex, columnArray[rowIndex]);
		}
	}
	
	public Fraction[] getColumn(int columnIndex) throws InputException {
		if (columnIndex < 0 || columnIndex >= this.columns) {
			throw new InputException("Index is out of bound!");
		}
		Fraction[] result = new Fraction[this.rows];
		for(int row = 0; row < this.rows; row++) {
			result[row] = this.getElement(row, columnIndex);
		}
		return result;
	}
	
	public void printMatrix() {
		for (Fraction[] row : this.array) {
			for (Fraction element : row) {
				System.out.print(element.toString() + " ");
			}
			System.out.println();
		}
	}

	public static Matrix add(Matrix A, Matrix B) throws InputException {
		if (A.columns() != B.columns() || A.rows() != B.rows()){
			throw new OperationException("Matrices don't have valid Dimensions for Addition");
		}
		Matrix result = new Matrix(A.rows(), A.columns());
		for (int row = 0; row < A.rows(); row++){
			for (int column = 0; column < A.columns(); column++) {
				result.setElement(row,column, Fraction.add(A.getElement(row,column), B.getElement(row,column)));
			}
		}
        return result;
	}

	public static Matrix mul(Matrix aMatrix, Fraction aScalar) throws InputException {
		Matrix result = new Matrix(aMatrix);
		for (int row = 0; row < aMatrix.rows(); row++){
			for (int column = 0; column < aMatrix.columns(); column++) {
				result.setElement(row,column,Fraction.mul(aMatrix.getElement(row,column), aScalar));
			}
		}
		return result;
	}

	public static Matrix mul(Matrix aMatrix, Matrix anotherMatrix) throws InputException {
		if (aMatrix.columns() != anotherMatrix.rows()) {
			throw new InputException("Matrices have invalid Dimensions.");
		}
		Matrix result = new Matrix(aMatrix.rows(), anotherMatrix.columns());

		for (int row = 0; row < result.rows(); row++) {
			for (int column = 0; column < result.columns(); column++) {
				Fraction element = new Fraction(0l, 1l);
				for (int step = 0; step < aMatrix.columns(); step++) {
					element = Fraction.add(element, Fraction.mul(aMatrix.getElement(row, step), anotherMatrix.getElement(step, column)));
				}
				result.setElement(row, column, element);
			}
		}
		return result;
	}


	public static Matrix id(int dimension) throws InputException {
		Matrix result = new Matrix(dimension, dimension);
		for (int i = 0; i < dimension; i++) {
			result.setElement(i,i, new Fraction(1l, 1l));
		}
		return result;
	}

	public int rank() {
	    this.ops.getRankAlgorithm().solve(this);
		return this.ops.getRankAlgorithm().getRank();
	}

	public Matrix inverse() {
		if (this.rows != this.columns) {
			throw new OperationException("Matrix is not quadratic.");
		}
		this.ops.getInvAlgorithm().solve(this);
		return this.ops.getInvAlgorithm().getInverse();
	}

	public Fraction det() {
		if (this.rows != this.columns) {
			throw new OperationException("Matrix is not quadratic.");
		}
		this.ops.getDetAlgorithm().solve(this);
		return this.ops.getDetAlgorithm().getDet();
	}

	public static Matrix solveLGS(Matrix coefficients, Matrix rhs) throws InputException {
		Fraction det_of_coefficient_matrix = coefficients.det();
		if (Fraction.isEqual(det_of_coefficient_matrix, new Fraction(0l, 1l))) {
			throw new InputException("This LGS does not have a clear solution!");
		}
		Matrix result = new Matrix(coefficients.rows(),1);

		for (int i = 0; i < coefficients.rows(); i++) {
		    Fraction x_i = Fraction.div(new Fraction(1l, 1l), det_of_coefficient_matrix);
			Fraction[] tmp = coefficients.getColumn(i);
			coefficients.setColumn(i,rhs.getColumn(0));
			x_i = Fraction.mul(x_i, coefficients.det());
			coefficients.setColumn(i,tmp);
			result.setElement(i,0,x_i);
		}

		return result;
	}
}
