package mts_math;

public class OperationException extends RuntimeException{

    public OperationException(String str) {
        super(str);
    }
}
