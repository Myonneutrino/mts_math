package mts_math;

public class InputException extends Exception {
    public InputException(String str) {
        super(str);
    }
}
