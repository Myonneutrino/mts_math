package mts_math;

public class Fraction {
    private long nominator;
    private long denominator;
    private static double EPSILON = 0.000001D;

    public Fraction(long nominator, long denominator) throws InputException {
        if (denominator == 0l) {
            throw new InputException("denominator cannot be zero!");
        }
        this.nominator = nominator;
        this.denominator = denominator;
        this.shorten();
        this.correct_sign();
    }

    public Fraction(double doubleNumber) {
        if (doubleNumber > -EPSILON && doubleNumber < EPSILON) {
            this.nominator = 0;
            this.denominator = 1;
        }
        String number = Double.toString(doubleNumber);
        String[] splitNumber = number.split("\\.");
        int decimals = splitNumber[1].length();
        long nominator = Long.parseLong((splitNumber[0] + splitNumber[1]));
        long denominator = (long) Math.pow(10D, (double) decimals);

        this.nominator = nominator;
        this.denominator = denominator;
        this.shorten();
        this.correct_sign();
    }

    public Fraction(String fractionString) {
        String[] fractionArray = fractionString.split("/");
        if (fractionArray.length == 1) {
            double value = Double.parseDouble(fractionString);
            Fraction fraction = new Fraction(value);
            this.nominator = fraction.getNominator();
            this.denominator = fraction.getDenominator();
        } else if (fractionArray.length == 2) {
            double value1 = Double.parseDouble(fractionArray[0]);
            double value2 = Double.parseDouble(fractionArray[1]);
            Fraction fraction1 = new Fraction(value1);
            Fraction fraction2 = new Fraction(value2);
            Fraction result = Fraction.div(fraction1, fraction2);
            this.nominator = result.getNominator();
            this.denominator = result.getDenominator();
        }
    }

    public Fraction(Fraction that) throws InputException {
        this(that.getNominator(), that.getDenominator());
    }

    private void shorten() {
        long gcd = euklid_gcd(this.nominator, this.denominator);
        this.nominator = this.nominator / gcd;
        this.denominator = this.denominator / gcd;
    }

    private void correct_sign() {
        if (this.denominator < 0) {
            this.denominator *= -1;
            this.nominator *= -1;
        }
    }

    private long euklid_gcd(long number1, long number2) {
        if (number1 == 0) {
            return number2;
        } else if (number2 == 0) {
            return number1;
        }
        long rest = number1 % number2;
        return euklid_gcd(number2, rest);
    }

    public long getNominator() {
        return this.nominator;
    }

    public long getDenominator() {
        return this.denominator;
    }

    public String toString() {
        if (this.denominator == 1L) {
            return "" + this.nominator;
        } else {
            return "" + this.nominator + "/" + this.denominator;
        }
    }

    public static Fraction add(Fraction fraction1, Fraction fraction2) {
        long nominator = fraction1.getNominator() * fraction2.getDenominator() + fraction2.getNominator() * fraction1.getDenominator();
        long denominator = fraction1.getDenominator() * fraction2.getDenominator();
        try {
            return new Fraction(nominator, denominator);
        } catch (InputException e) {
            // should never happen
            return null;
        }
    }

    public static Fraction mul(Fraction fraction1, Fraction fraction2) {
        long nominator = fraction1.getNominator() * fraction2.getNominator();
        long denominator = fraction1.getDenominator() * fraction2.getDenominator();
        try {
            return new Fraction(nominator, denominator);
        } catch (InputException e) {
            //should never happen.
            return null;
        }
    }

    public static Fraction mul(Fraction fraction, double doubleNumber) throws InputException {
        long nominator = fraction.getNominator() * (long) doubleNumber;
        return new Fraction(nominator, fraction.getDenominator());
    }

    public static Fraction sub(Fraction fraction1, Fraction fraction2) {
        long nominator = fraction1.getNominator() * fraction2.getDenominator() - fraction2.getNominator() * fraction1.getDenominator();
        long denominator = fraction1.getDenominator() * fraction2.getDenominator();
        try {
            return new Fraction(nominator, denominator);
        } catch (InputException e) {
            // should never happen
            return new Fraction(0);
        }
    }

    public static Fraction div(Fraction fraction1, Fraction fraction2) {
        long nominator = fraction1.getNominator() * fraction2.getDenominator();
        long denominator = fraction1.getDenominator() * fraction2.getNominator();
        try {
            return new Fraction(nominator, denominator);
        } catch (InputException e) {
            //should never happen.
            return null;
        }
    }

    public static Fraction pow(Fraction basis, long exponent) {
        long nominator = (long) Math.pow(basis.getNominator(), exponent);
        long denominator = (long) Math.pow(basis.getDenominator(), exponent);
        try {
            return new Fraction(nominator, denominator);
        } catch (InputException e) {
            // should never happen
            return null;
        }
    }

    public static double pow(Fraction fraction1, Fraction fraction2) {
        double basis = (double) fraction1.getNominator() / (double) fraction1.getDenominator();
        double pow = Math.pow(basis, (double) fraction2.getNominator());
        double root = Math.pow(pow, 1D / (double) fraction2.getDenominator());
        return root;
    }

    public static double sqrt(Fraction fraction1) {
        try {
            return Fraction.pow(fraction1, new Fraction(1l, 2l));
        } catch (InputException e) {
            // cannot happen: 1l != 0
            return 0;
        }
    }

    public static double toDouble(Fraction fraction) {
        return (double) fraction.getNominator() / (double) fraction.getDenominator();
    }

    public static int toInt(Fraction fraction) {
        return (int) (fraction.getNominator() / fraction.getDenominator());
    }

    public static long toLong(Fraction fraction) {
        return (long) (fraction.getNominator() / fraction.getDenominator());
    }

    public static String toString(Fraction fraction) {
        if (fraction.getDenominator() == 1L) {
            return "" + fraction.getNominator();
        } else {
            return "" + fraction.getNominator() + "/" + fraction.getDenominator();
        }
    }

    public static boolean isEqual(Fraction fraction1, Fraction fraction2) {
        if (fraction1.getNominator() == fraction2.getNominator() && fraction1.getDenominator() == fraction2.getDenominator()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isGreaterThan(Fraction fraction1, Fraction fraction2) {
        if (Fraction.toDouble(fraction1) > Fraction.toDouble(fraction2)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSmallerThan(Fraction fraction1, Fraction fraction2) {
        if (Fraction.toDouble(fraction1) < Fraction.toDouble(fraction2)) {
            return true;
        } else {
            return false;
        }
    }

}
