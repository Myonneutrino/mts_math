package mts_math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    public static double delta = 0.0001;

    void assertMatrixEquals(Matrix expected, Matrix actual) throws InputException{
        assertEquals(actual.rows(), expected.rows());
        assertEquals(actual.columns(), expected.columns());
        for (int row = 0; row < actual.rows() ; row++) {
            for (int column = 0; column < actual.columns(); column++) {
                assertFractionEquals(expected.getElement(row, column), actual.getElement(row, column));
            }
        }
    }

    void assertFractionEquals(Fraction expected, Fraction actual) {
        double exp = (double) expected.getNominator() / (double) expected.getDenominator();
        double act = (double) actual.getNominator() / (double) actual.getDenominator();

        assertEquals(exp, act, delta);
    }

    @Test
    void setGetElement() throws InputException {
        Matrix A = new Matrix(3,3);
        Fraction newValue = new Fraction(13.5D);
        A.setElement(2,0, newValue);
        assertFractionEquals(A.getElement(2,0), newValue);
    }

    @Test
    void setGetRow() throws InputException {
        Matrix A = new Matrix(3,3);
        Fraction[] newRow = {new Fraction(4),new Fraction(5), new Fraction(6)};
        A.setRow(1,newRow);
        assertArrayEquals(newRow,A.getRow(1));
    }

    @Test
    void setGetColumn() throws InputException {
        Matrix A = new Matrix(3,3);
        Fraction[] newColumn = {new Fraction(1), new Fraction(4), new Fraction(7)};
        A.setColumn(0, newColumn);
        assertArrayEquals(newColumn, A.getColumn(0));
    }

    @Test
    void Constructor() throws InputException {
        double[][] matrix = {{1D,2D,3D},{4D, 5D, 6D},{7D,8D,9D}};
        Matrix A = new Matrix(matrix);
        assertFractionEquals(new Fraction(5), A.getElement(1,1));
    }

    @Test
    public void wrongConstructorParameters() {
        double[][] a = {};
        double[][] b = {{},{},{}};
        double[][] c = {{1,2,3},{1,2,3},{1,2,3,4}};
        double[][] d = {{1,2,3},{},{}};

        assertThrows(InputException.class, () -> new Matrix(a));
        assertThrows(InputException.class, () -> new Matrix(b));
        assertThrows(InputException.class, () -> new Matrix(c));
        assertThrows(InputException.class, () -> new Matrix(d));
        assertThrows(InputException.class, () -> new Matrix(-2,-1));
        assertThrows(InputException.class, () -> new Matrix(0, 0));
    }

    @Test
    public void invalidAccessorInputs() throws InputException {
        Fraction nullFrac = new Fraction(0);
        Matrix M = new Matrix(new double[][] {{1,2,3},{1,2,3},{1,2,3}});
        assertThrows(InputException.class, () -> M.setElement(-1,1,new Fraction(10D)));
        assertThrows(InputException.class, () -> M.setElement(3,1,new Fraction(10D)));
        assertThrows(InputException.class, () -> M.getElement(-1,1));
        assertThrows(InputException.class, () -> M.getElement(3,1));
        assertThrows(InputException.class, () -> M.setRow(0, new Fraction[] {nullFrac,nullFrac,nullFrac,nullFrac}));
        assertThrows(InputException.class, () -> M.setRow(-1, new Fraction[] {nullFrac,nullFrac,nullFrac}));
        assertThrows(InputException.class, () -> M.setRow(3, new Fraction[] {nullFrac,nullFrac,nullFrac}));
        assertThrows(InputException.class, () -> M.getRow(-1));
        assertThrows(InputException.class, () -> M.getRow(3));
        assertThrows(InputException.class, () -> M.setColumn(0, new Fraction[] {nullFrac,nullFrac,nullFrac,nullFrac}));
        assertThrows(InputException.class, () -> M.setColumn(-1, new Fraction[] {nullFrac,nullFrac,nullFrac}));
        assertThrows(InputException.class, () -> M.setColumn(3, new Fraction[] {nullFrac,nullFrac,nullFrac}));
        assertThrows(InputException.class, () -> M.getColumn(-1));
        assertThrows(InputException.class, () -> M.getColumn(3));
    }

    @Test
    public void printMatrix() throws InputException {
        Matrix A = new Matrix(new double[][] {{1D,2D,3D},{1D,2D,3D},{1D,2D,3D}});
        A.printMatrix();
    }

    @Test
    void add() throws InputException {
        Matrix A = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
        Matrix B = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
        Matrix C = new Matrix(new double[][] {{1,2,3,0},{4,5,6,0},{7,8,9,0}});
        Matrix D = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}, {10,11,12}});

        Matrix result = new Matrix(new double[][] {{2,4,6},{8,10,12},{14,16,18}});
        assertMatrixEquals(result, Matrix.add(A,B) );
        assertThrows(OperationException.class, () -> Matrix.add(A,C));
        assertThrows(OperationException.class, () -> Matrix.add(A,D));
    }

    @Test
    void scalarMul() throws InputException {
        Matrix A = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
        Fraction a = new Fraction(10D);
        Matrix Result = new Matrix(new double[][] {{10,20,30},{40,50,60},{70,80,90}});
        assertMatrixEquals(Result, Matrix.mul(A,a));
    }

    @Test
    void MatrixMul() throws InputException {
        Matrix _3x3 = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
        Matrix _3x4 = new Matrix(new double[][] {{1,2,3,4},{4,5,6,4},{7,8,9,4}});
        Matrix _4x3 = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9},{0,5,10}});
        Matrix _4x4 = new Matrix(new double[][] {{1,2,3,4},{4,5,6,4},{7,8,9,4}, {0,47,11,23}});
        Matrix id3 =  new Matrix(new double[][] {{1,0,0},{0,1,0},{0,0,1}});
        Matrix id4 =  new Matrix(new double[][] {{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}});

        Matrix result_3x3_3x3 = new Matrix(new double[][] {{30,36,42},{66,81,96},{102,126,150}});
        Matrix result_3x3_3x4 = new Matrix(new double[][] {{30,36,42,24},{66,81,96,60},{102,126,150,96}});
        Matrix result_4x3_3x4 = new Matrix(new double[][] {{30,36,42,24},{66,81,96,60},{102,126,150,96},{90,105,120,60}});
        Matrix result_3x4_4x3 = new Matrix(new double[][] {{30,56,82},{66,101,136},{102,146,190}});
        Matrix result_id3_3x4 = new Matrix(new double[][] {{1,2,3,4},{4,5,6,4},{7,8,9,4}});
        Matrix result_3x4_id4 = new Matrix(new double[][] {{1,2,3,4},{4,5,6,4},{7,8,9,4}});

        assertThrows(InputException.class, () -> Matrix.mul(_3x3, _4x3));

        assertMatrixEquals(Matrix.mul(_3x3, _3x3), result_3x3_3x3);
        assertMatrixEquals(Matrix.mul(_3x3, _3x4), result_3x3_3x4);
        assertMatrixEquals(Matrix.mul(_4x3, _3x4), result_4x3_3x4);
        assertMatrixEquals(Matrix.mul(_3x4, _4x3), result_3x4_4x3);
        assertMatrixEquals(Matrix.mul(id3, _3x4), result_id3_3x4);
        assertMatrixEquals(Matrix.mul(_3x4, id4), result_3x4_id4);
    }

    @Test
    void rank() throws InputException {
        Matrix A = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
        assertEquals(2, A.rank());

        Matrix B = new Matrix(new double[][] {{1,2,3},{1,2,3},{1,2,3}});
        assertEquals(1, B.rank());

        Matrix C = new Matrix(new double[][] {
                { -77,  -70,  89, -214, -169,   16},
                { 297, -529, 278, -290,  527,  279},
                {-435,  330, -33, -174, -759, -148}});
        assertEquals(3, C.rank());
    }

    @Test
    void det() throws InputException {
        Matrix A = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
        Matrix B = new Matrix(new double[][] {{1,2,3},{0,5,6},{7,8,0}});
        Matrix C = new Matrix(new double[][] {{ -77,  -70,  89},{ 297, -529, 278},{-435,  330, -33}});
        assertFractionEquals(new Fraction(0D), A.det());
        assertFractionEquals(new Fraction(-69D), B.det());
        assertFractionEquals(new Fraction(1741476), C.det());

        Matrix D = new Matrix(new double[][] {
                { -77,  -70,  89, -214, -169,   16},
                { 297, -529, 278, -290,  527,  279},
                {-435,  330, -33, -174, -759, -148}});
        assertThrows(OperationException.class, () -> D.det());
    }

    @Test
    void inverse() throws InputException {
        Matrix B = new Matrix(new double[][] {{1,2,3},{0,5,6},{7,8,0}});
        Matrix B_inverse = Matrix.mul(new Matrix(new double[][] {{48D,-24D,3D},{-42D,21D,6D},{35D,-6D,-5D}}), new Fraction("1/69"));
        Matrix C = new Matrix(new double[][] {{ -77,  -70,  89},{ 297, -529, 278},{-435,  330, -33}});
        Matrix C_inverse = Matrix.mul(new Matrix(new double[][] {
                { -74283,27060,27621},
                {-111129,41256,47839},
                {-132105,55860,61523}}), new Fraction("1/1741476"));
        assertMatrixEquals(B_inverse, B.inverse());
        assertMatrixEquals(C_inverse, C.inverse());
        assertMatrixEquals(Matrix.id(5), Matrix.id(5).inverse());

        Matrix A = new Matrix(new double[][] {{1,2,3},{0,5,6},{7,8,0},{7,8,0}});
        assertThrows(OperationException.class, () -> A.inverse());

        Matrix D = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
        assertThrows(OperationException.class, () -> D.inverse());
    }

//    @Test
//    void switchRows() throws InputException {
//        Matrix A = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
//        A.switchRows(0,2);
//        Matrix result = new Matrix(new double[][] {{7,8,9},{4,5,6},{1,2,3}});
//        assertMatrixEquals(result,A);
//        assertThrows(InputException.class, () -> A.switchRows(-1,1));
//        assertThrows(InputException.class, () -> A.switchRows(0,3));
//    }
//
//    @Test
//    void addMultipleOfRow() throws InputException {
//        Matrix A = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
//        A.addMultipleOfRow(0,1,-4D);
//        Matrix result = new Matrix(new double[][] {{1,2,3},{0,-3,-6},{7,8,9}});
//        assertMatrixEquals(result, A);
//        assertThrows(InputException.class, () -> A.addMultipleOfRow(-1,1,15));
//        assertThrows(InputException.class, () -> A.addMultipleOfRow(0,3,15));
//    }
//
//    @Test
//    void multiplyRow() throws InputException {
//        Matrix A = new Matrix(new double[][] {{1,2,3},{4,5,6},{7,8,9}});
//        A.multiplyRow(0,10D);
//        Matrix result = new Matrix(new double[][] {{10,20,30},{4,5,6},{7,8,9}});
//        assertMatrixEquals(result, A);
//        assertThrows(InputException.class, () -> A.multiplyRow(-1, 10D));
//        assertThrows(InputException.class, () -> A.multiplyRow(3, 10D));
//    }

    @Test
    void solveLGS() throws InputException {
        // Arrange
        Matrix C = new Matrix(new double[][] {
                { -77,  -70,  89},
                { 297, -529, 278},
                {-435,  330, -33}});
        Matrix rhs = new Matrix(new double[][] {
                { 1},
                { 2},
                { 3}
        });
        Matrix expected_solution = new Matrix(new double [][] {
                { 0.036004},
                { 0.065979},
                { 0.094279}
        });

        // Act
        Matrix result = Matrix.solveLGS(C, rhs);

        // Assert
        assertMatrixEquals(expected_solution,result);
    }

}