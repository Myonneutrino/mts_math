package mts_math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FractionTest {
    @Test
    void fraction() throws InputException {
        // Arrange
        Fraction sut = new Fraction(36L, 6L);
        Fraction sut0 = new Fraction(0L, 127L);

        // Act

        //Assert
        assertEquals(6L, sut.getNominator());
        assertEquals(1L, sut.getDenominator());

        assertEquals(0L, sut0.getNominator());
        assertEquals(1L, sut0.getDenominator());
    }

    @Test
    void fraction_double() throws InputException {
        // Arrange
        Fraction sut = new Fraction(0.1234D);
        Fraction expected = new Fraction(1234L, 10000L);

        // Assert
        assertEquals(expected.getNominator(), sut.getNominator());
        assertEquals(expected.getDenominator(), sut.getDenominator());
    }

    @Test
    void fraction_String() throws InputException {
        // Arrange
        Fraction sut1 = new Fraction("1/4");
        Fraction sut2 = new Fraction("0.25/0.75");

        // Assert
        assertEquals(1L, sut1.getNominator());
        assertEquals(4L, sut1.getDenominator());

        assertEquals(1L, sut2.getNominator());
        assertEquals(3L, sut2.getDenominator());
    }

    @Test
    void add() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(2L, 3L);

        // Act
        Fraction sut = Fraction.add(fraction1, fraction2);

        //Assert
        assertEquals(7L, sut.getNominator());
        assertEquals(6L, sut.getDenominator());
    }

    @Test
    void mul() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(3L, 2L);
        Fraction fraction2 = new Fraction(2L, 3L);

        // Act
        Fraction sut = Fraction.mul(fraction1, fraction2);

        //Assert
        assertEquals(1L, sut.getNominator());
        assertEquals(1L, sut.getNominator());
    }

    @Test
    void sub() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(2L, 3L);

        // Act
        Fraction sut = Fraction.sub(fraction1, fraction2);

        //Assert
        assertEquals(-1L, sut.getNominator());
        assertEquals(6L, sut.getDenominator());
    }

    @Test
    void div() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(2L, 3L);

        // Act
        Fraction sut = Fraction.div(fraction1, fraction2);

        //Assert
        assertEquals(3L, sut.getNominator());
        assertEquals(4L, sut.getDenominator());
    }

    @Test
    void pow() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(2L, 1L);

        Fraction fraction3 = new Fraction(9L, 16L);
        Fraction fraction4 = new Fraction(1L, 2L);

        // Act
        double sut1 = Fraction.pow(fraction1, fraction2);
        double sut2 = Fraction.pow(fraction3, fraction4);

        //Assert
        assertEquals(0.25D, sut1, 0.000000001);

        assertEquals(0.75D, sut2, 0.000000001);
    }

    @Test
    void sqrt() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(9L, 16L);
        Fraction fraction2 = new Fraction(2L, 1L);

        Fraction fraction3 = new Fraction(1L, 2L);
        Fraction fraction4 = new Fraction(1L, 2L);

        // Act
        double sut1 = Fraction.sqrt(fraction1);

        //Assert
        assertEquals(0.75D, sut1);
    }

    @Test
    void toDouble() throws InputException {
        // Arrange
        Fraction fraction = new Fraction(2L, 5L);

        // Act
        double sut = Fraction.toDouble(fraction);

        //Assert
        assertEquals(0.4D, sut, 0.000000001D);
    }

    @Test
    void toInt() throws InputException {
        // Arrange
        Fraction fraction = new Fraction(12L, 6L);

        // Act
        int sut = Fraction.toInt(fraction);

        //Assert
        assertEquals(2, sut);
    }

    @Test
    void testToString() throws InputException {
        // Arrange
        Fraction fraction = new Fraction(7L, 13L);

        // Act
        String sut = Fraction.toString(fraction);

        //Assert
        assertEquals("7/13", sut);
    }

    @Test
    void isEqual() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(2L, 4L);

        // Act
        boolean isEqual = Fraction.isEqual(fraction1, fraction2);

        //Assert
        assertEquals(true, isEqual);
    }

    @Test
    void isNotEqual() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(3L, 4L);

        // Act
        boolean isEqual = Fraction.isEqual(fraction1, fraction2);

        //Assert
        assertEquals(false, isEqual);
    }

    @Test
    void isGreaterThan() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(3L, 4L);

        // Act
        boolean sut1 = Fraction.isGreaterThan(fraction1, fraction2);
        boolean sut2 = Fraction.isGreaterThan(fraction2, fraction1);
        boolean sut3 = Fraction.isGreaterThan(fraction1, fraction1);

        //Assert
        assertEquals(false, sut1);
        assertEquals(true, sut2);
        assertEquals(false, sut3);
    }

    @Test
    void isSmallerThan() throws InputException {
        // Arrange
        Fraction fraction1 = new Fraction(1L, 2L);
        Fraction fraction2 = new Fraction(3L, 4L);

        // Act
        boolean sut1 = Fraction.isSmallerThan(fraction1, fraction2);
        boolean sut2 = Fraction.isSmallerThan(fraction2, fraction1);
        boolean sut3 = Fraction.isSmallerThan(fraction1, fraction1);

        //Assert
        assertEquals(true, sut1);
        assertEquals(false, sut2);
        assertEquals(false, sut3);
    }
}
