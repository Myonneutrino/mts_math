* Matrix-Klasse(Python)
** Motivation
Die Motivation ist zum einen eine Hausaufgabe aus dem LF5 meiner Berufsausbildung und zum anderen mein Anspruch eine
funktionale Matrizen-Klasse zu implementieren. Dazu geht es noch um das Lernen von ein paar generellen Aspekten der
Entwicklung, wie zB. Testing, Exception Handling und Dokumentation.
** Anforderungen
*** Anforderungen der Hausaufgabe
**** Matrixmultiplikation
**** skalare Multiplikation
**** Matrixaddition
**** skalare Addition
**** Bestimmung des Rangs einer Matrix
**** Dokumentation
**** Tests
**** Exception Handling
**** Klassendiagramme
*** Zusätzliche selbstauferlegte Anforderungen
**** Implementierung von operator overloading und magic methods als sinnvolle Ergänzung der Klasse (Python-speziefisch)
***** __getitem__ und __setitem__ um die Elemente in einer Matrix M mit M[j,i] zu bekommen und zu setzen
***** __str__ und __repr__ um der Matrix nützliche String-Repräsentationen zu geben. (__len__ als Hilfsfunktion)
***** __iter__ und __eq__ um über die Einträge der Matrix bequem iterieren zu können und Matrizen auf Gleichheit zu prüfen.
***** __add__,__radd__,__sub__,__rsub__,__mul__,__rmul__, sowie __pow__ um die Standardoperatoren +,-,* bequem nutzen zu können.
**** Treppennormalform für beliebige Matrizen herstellen
***** einmal durch Matrizenmultiplikation und
***** einmal so effizient wie mir möglich
**** statische Factory-Methoden für gängige Matrizen (Permutation, Matrizen für Gauß-Elimination, Identität)
**** Potenzierung von Matrizen
***** speziell auch für negative ganzzahlige Exponenten durch das bilden der Inversen.
**** Berechnung der Inversen
**** Berechnung der Determinante
**** Transponieren einer Matrix
** Herangehensweise
Meine Herangehensweise hier ist als erstes die Anforderungen umzusetzen, die ich bereits kann oder die hauptsächlich
eine Übung darstellen und anschließend solche Anforderungen, für die ich mir noch Kenntnisse oder Methoden aneignen muss.
Das heißt konkret: Zuerst die Funktionalitäten der Matrix-Klasse. Und im Anschluss die Dokumentation, das Exception-Handling,
Das Testen und die Klassendiagramme.
Um die Funktionalität meiner Klasse in jeder Iteration zu gewährleisten, führe ich manuelle Tests mit dem interaktiven
Python Interpreter durch.
** warum Python?
Generell fühle ich mich wohler mit Python. Allerdings sehe ich in diesem Anwendungsfall den großen Vorteil der interaktiven
Kommandozeile der Python Interpreters. Hier kann die Klasse importiert werden und direkt wie mit einem Taschenrechner
gerechnet werden ohne eine Aufwändige GUI/TUI zu bauen.
Dazu gibt es in Python Operator-Overloading und magic methods, was das Programmieren und benutzen der fertigen Klasse 
wesentlich bequemer macht.
Ein weiterer Vorteil ist der in Python interne Umgang mit (großen) Zahlen. Dazu gibt es praktische packages wie z.B. Brüche
mit denen ebenfalls wesentlich präziser gerechnet werden kann.
Dazu lerne ich aktuell lieber in Python dazu als in anderen Sprachen.
** Hilfsmittel
Als Hilfsmittel bezeichne ich hier Programme und Module, die ich zum Schreiben der Klasse, zum Anfertigen von Diagrammen
oder für einzelne Funktionalitäten der Klasse benutze. Das sind:
*** "Numbers" aus dem "numbers"-Modul, die als Superklasse für die meisten (guten) Zahlenklassen in Python fungiert.
*** "deepcopy" aus dem "copy"-Modul um in den Algorithmen nicht die Ursprungsobjekte zu verändern.
*** Fraction" aus dem "fractions"-Modul um der Ungenauigkeit von float-Berechnungen entgegenzuwirken.
*** git als Versionierungssoftware, github als remote
*** spacemacs als Entwicklungsumgebung
*** draw.io zum Anfertigen der UML-Klassendiagramme
*** das Modul "unittest" zum Entwickeln der Automatisierten Tests.
*** Wolfram Alpha zum verifizieren von Lösungen und generieren von Testwerten
** Anwender-Dokumentation
Die Peer-Dokumentation wird noch erstellt. Geplant ist diese in-line zu erstellen um mit Hilfe der help() Methode
in der interaktiven Pyhton-Shell eine hilfreiche Dokumentation zu bekommen.

** Herausforderungen, Projekt-Risiken
** aktuelle TODO's
* implementierte Features, Kommentare




* Matrix-Klasse(Java)
** Motivation
Anlass ist eine Hausaufgabe aus dem Beruffsschulunterricht. 
** Anforderungen
*** Anforderungen der Hausaufgabe
**** Matrixmultiplikation
**** skalare Multiplikation
**** Matrixaddition
**** skalare Addition
**** Bestimmung des Rangs einer Matrix
**** Dokumentation
**** Tests
**** Exception Handling
**** Klassendiagramme
*** Selbstauferlegte Anforderungen
**** Treppennormalform herstellen
**** Berechnung der Inversen
**** Berechnung der Determinanten
**** Transponieren einer Matrix
**** spezielle Matrizen erstellen
**** evtl. TUI
** Herangehensweise
Diesmal is die Idee parallel die Dokumentation, die Tests und das Exception Handling zu implementieren. Da hier vieles 
für mich neu ist, können die ersten Iterationen dabei auch länger dauern. Ich nehme an, dass sich dieser Prozess dann
allerdings nach hinten hin beschleunigen wird.
Generell geht es hier weniger darum etwas über die Mathematik oder Algorithmen zu lernen, sondern hauptsächlich um 
Workflow, Sprachfeatures, Übung und darum selbst Herausforderungen zu suchen.
** Warum Java?
Ich benutze hier Java um eine Sprache zu benutzen mit der ich noch nicht so "vertraut" bin. Dazu ist diese Relevant
für den Berufsschulunterricht mindestens im ersten Lehrjahr. Dazu kann ich hier erlangte Fähigkeiten und Kenntnisse
auch für andere OOP-fokussierte Sprachen wiederanwenden.
Die Wahl der Sprache Java ist hier also nicht aus pragmatischen Gründen oder weil ich denke, dass sie besonders dafür
geeignet ist, sondern um dazuzulernen.
** Hilfsmittel
Hier werden im Laufe der Programmierung alle Hilfsmittel, teilweise mit Rechtfertigungen, auftauchen.
*** Eclipse als Entwicklungsumgebung
*** git als Versionierungssoftware mit Bitbucket als remote
*** draw.io zum Anfertigen der UML-Klassendiagramme
*** Wolfram Alpha zum verifizieren von Lösungen und generieren von Testwerten für die Testcases
** Anwender Dokumentation
Werde ich versuchen als letztes zu implementieren. Noch kenne ich auch nicht die geeignetste Methode dafür.
** Herausforderungen, Projekt-Risiken
Die Herausforderung wird als erstes schonmal mein kleiner Kenntnisstand der Sprache Java sein. Dazu kommt, dass mir
Teilbereiche der Anforderungen mir noch unbekannt sind. Dazu zählt zB. Exception Handling, gute Dokumentation und
Testing Frameworks.
** aktuelle TODO's
*** Projektstart, also erste Konstruktion der Klasse
*** Architektur für die Anforderungen überlegen
* allgemeine Kommentare zu den Features, Arbeitsschritten, dem Fortschritt
** Das Projekt ist angelegt, das git-repo erstellt.
Nun geht es um das weitere Verfahren. Welche Methodik will ich fahren? Ich versuche den ersten Teil im TDD-Style 
zu entwickeln. Also erstes Feature, dazu Tests schreiben, implementieren, Testen.
** Das erste Feature: die Konstruktion
*** Wie kann ich die Konstruktion meiner Matrix testen? -> JUnit: Wie benutze ich JUnit?
noch unklar. Aber auch unnötig, da man hier durch diverse End-to-End Tests das Resultat bekommt.
*** Wie soll der Konstruktor überhaupt funktionieren?
Wir können verschiedene Konstruktoren erstellen, daher erstelle ich erstmal einen Konstruktor, der ein 2-dimensionales Array von doubles erwartet.
Dazu einen für das erstellen einer mxn 0-Matrix und einer nxn 0-Matrix.

Wir haben hier verschiedene Konstruktoren mit verschiedenen Inputs:
**** 2-dimensionales Array: direkte Deklaration aller einträge
**** ein integer: quadratische 0-Matrix
**** zwei integer: nxm-0-Matrix
** Wie soll die Projekt Architektur aussehen?
Denkbar wären zwei Ansätze:
   Ich wähle den ersten Anstatz.
*** Eine einzige Matrixklasse mit Multiplikations-, Additions-, Transpositions-, Determinanten-, Inversen-, etc. Methoden
*** Mehrere Klassen, wobei die Matrix als solche nur eine Art primitiver Datentyp ist und die Rechenoperationen von einer Taschenrechnerklasse durchgeführt werden
Ich entscheide mich hier für den 1. Ansatz in Anlehnung an andere populäre Java-Mathe Klassen (z.B. BigInteger). Dabei soll dann also eine Matrix M nach ihrer Instantiierung z.B. durch M.treppenNormalForm() selbst
ihre eigene Treppennormalform berechnen und ausgeben. Ob das sinnvoll ist oder nicht, kann ich (noch) nicht beurteilen.
** Getter und Setter
Erledigt. Die getter/setter und Konstruktoren sind implementiert und mit Exceptions sowie Tests versehen.
*** get/set matrix
*** get/set row/column
*** get/set element

** Testing
   JUnit Tests für die Getter und Setter
   Danach Unit Tests vor den und für die Berechnungsfeatures (TDD)
*** Junit 5.6 ist als Testframework eingerichtet.
** Exception Handling
Exceptions Handling (throwing) ist implementiert, allerdings ist mir noch unklar, ob ich checked oder unchecked Exceptions benutzen
sollte.
** Berechnungen auf/mit Matrizen
*** statische Methoden für Berechnungen auf den Matrizen
**** det, inv, add, sub, mul, tnf, rank
** Erstelle JavaDocs
* implementierte Features
** [x] add(aMatrix, anotherMatrix): Elementweise Addition zweier Matrizen.
** [x] add(aMatrix, aDouble): Addition von Skalar und Matrix, elementar.
** [x] mul(aMatrix, aDouble): Multiplikation von Skalar und Matrix
** [x] mul(aMatrix, anotherMatrix): Matrixmultiplikation
** [x] rank(aMatrix): Bestimmen des Ranks einer Matrix.
   Dazu gibt es ein paar nützliche Hilfsfunktionen:
*** Zeilen Tauschen
*** Zeile mit Skalar multiplizieren
*** Vielfaches einer Zeile auf eine andere multiplizieren
*** größtes Element einer Spalte finden
*** 
